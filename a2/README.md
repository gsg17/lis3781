> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Gabriel Gomez

### A2 # Requirements:

*Sub-Heading:*

1. Ordered-list items
2. 
3. 

#### README.md file should include the following items:

* Bullet-list items
* 
* 
* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1.git init - Creates a new git repository 
2.git status - Shows current status of working directory 
3.git add - Adds all new files in current directory, prepared for git commit
4.git commit - Commits all changes to local repository 
5.git push - Uploads local repo content into a remote repository 
6.git pull - Retrieves and downloads content from a remote repo
7.git log - Shows all changes made 

#### Assignment Screenshots:

*Screenshot of Code for Company[http://localhost:8080 "PHP Localhost"*:

![Company Code](img/Code1.png)

*Screenshot of Code for Customer*:

![Code for Customer Table](img/Code2.png)

*Screenshot of Code for Customer Cont*:

![Code for Customer Table Cont](img/Code3.png)

*Screenshot of Populated tables:

![Populated Customer and Company table](img/PopTables.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
