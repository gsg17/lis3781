> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Gabriel Gomez

### A4 # Requirements:

*Requirements:*

1. Screenshots of Populated Tables


#### README.md file should include the following items:

* Screenshot of populated tables


> 
#### Assignment Screenshots:

*Screenshot of Populated tables on SQL Server*:

![PopulatedTables](img/CODE1.png)

*Screenshot of Populated tables on SQL Server CONT*:

![PopulatedTables](img/CODE2.png)
