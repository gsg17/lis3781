-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema gsg17
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `gsg17` ;

-- -----------------------------------------------------
-- Schema gsg17
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gsg17` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `gsg17` ;

-- -----------------------------------------------------
-- Table `gsg17`.`job`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gsg17`.`job` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gsg17`.`job` (
  `job_id` TINYINT NOT NULL AUTO_INCREMENT,
  `job_title` VARCHAR(45) NOT NULL,
  `job_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`job_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gsg17`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gsg17`.`employee` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gsg17`.`employee` (
  `emp_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `job_id` TINYINT NOT NULL,
  `emp_ssn` INT UNSIGNED ZEROFILL NOT NULL,
  `emp_fname` VARCHAR(15) NOT NULL,
  `emp_lname` VARCHAR(30) NOT NULL,
  `emp_dob` DATE NOT NULL,
  `emp_start_date` DATE NOT NULL,
  `emp_end_date` DATE NULL,
  `emp_salary` DECIMAL(8,2) NOT NULL,
  `emp_street` VARCHAR(30) NOT NULL,
  `emp_city` VARCHAR(20) NOT NULL,
  `emp_state` CHAR(2) NOT NULL,
  `emp_zip` INT UNSIGNED ZEROFILL NOT NULL,
  `emp_phone` BIGINT NOT NULL,
  `emp_email` VARCHAR(100) NOT NULL,
  `emp_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`emp_id`),
  INDEX `fk_employee_job1_idx` (`job_id` ASC),
  UNIQUE INDEX `emp_ssn_UNIQUE` (`emp_ssn` ASC),
  CONSTRAINT `fk_employee_job1`
    FOREIGN KEY (`job_id`)
    REFERENCES `gsg17`.`job` (`job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gsg17`.`emp_hist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gsg17`.`emp_hist` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gsg17`.`emp_hist` (
  `eht_id` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NULL,
  `eht_date` DATETIME NOT NULL,
  `eht_type` ENUM('i', 'u', 'd') NOT NULL,
  `eht_job_id` TINYINT NOT NULL,
  `eht_emp_salary` DECIMAL(8,2) NOT NULL,
  `eht_usr_changed` VARCHAR(30) NOT NULL,
  `eht_reason` VARCHAR(45) NOT NULL,
  `eht_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`eht_id`),
  INDEX `fk_emp_hist_employee1_idx` (`emp_id` ASC),
  CONSTRAINT `fk_emp_hist_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `gsg17`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gsg17`.`dependent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gsg17`.`dependent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gsg17`.`dependent` (
  `dep_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT NOT NULL,
  `dep_added` DATE NOT NULL,
  `dep_ssn` INT UNSIGNED ZEROFILL NOT NULL,
  `dep_fname` VARCHAR(15) NOT NULL,
  `dep_lname` VARCHAR(30) NOT NULL,
  `dep_dob` DATE NOT NULL,
  `dep_relation` VARCHAR(20) NOT NULL,
  `dep_street` VARCHAR(30) NOT NULL,
  `dep_city` VARCHAR(20) NOT NULL,
  `dep_state` CHAR(2) NOT NULL,
  `dep_zip` INT ZEROFILL UNSIGNED NOT NULL,
  `dep_phone` BIGINT NOT NULL,
  `dep_email` VARCHAR(100) NULL,
  `dep_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`dep_id`),
  INDEX `fk_dependent_employee_idx` (`emp_id` ASC),
  UNIQUE INDEX `dep_ssn_UNIQUE` (`dep_ssn` ASC),
  CONSTRAINT `fk_dependent_employee`
    FOREIGN KEY (`emp_id`)
    REFERENCES `gsg17`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gsg17`.`benefit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gsg17`.`benefit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gsg17`.`benefit` (
  `ben_id` TINYINT NOT NULL AUTO_INCREMENT,
  `ben_name` VARCHAR(45) NOT NULL,
  `ben_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`ben_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gsg17`.`plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gsg17`.`plan` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gsg17`.`plan` (
  `pln_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT NOT NULL,
  `ben_id` TINYINT NOT NULL,
  `pln_type` ENUM('singe', 'spouse', 'family') NOT NULL,
  `pln_cost` DECIMAL(6,2) NOT NULL,
  `pln_election_date` DATE NOT NULL,
  `pln_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pln_id`),
  INDEX `fk_plan_employee1_idx` (`emp_id` ASC),
  INDEX `fk_plan_benefit1_idx` (`ben_id` ASC),
  CONSTRAINT `fk_plan_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `gsg17`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plan_benefit1`
    FOREIGN KEY (`ben_id`)
    REFERENCES `gsg17`.`benefit` (`ben_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `gsg17`.`job`
-- -----------------------------------------------------
START TRANSACTION;
USE `gsg17`;
INSERT INTO `gsg17`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'Manager', NULL);
INSERT INTO `gsg17`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'Accountant', NULL);
INSERT INTO `gsg17`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'IT', NULL);
INSERT INTO `gsg17`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'Graphics', NULL);
INSERT INTO `gsg17`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'Designer', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `gsg17`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `gsg17`;
INSERT INTO `gsg17`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 1, 938574928, 'Orion', 'Constant', '1990-01-01', '2005-01-01', NULL, 22111, 'Tech Rd', 'Tampa', 'FL', 685432789, 8583943950, 'contact@gmail.com', NULL);
INSERT INTO `gsg17`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 2, 294394593, 'Jenny', 'Block', '1990-01-03', '2005-01-02', NULL, 54342, 'Monopoly Ln', 'Orlando', 'FL', 654324321, 4565432111, 'oreo@gmail.com', NULL);
INSERT INTO `gsg17`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 3, 485834031, 'Steve', 'Rogers', '1990-01-02', '2010-01-01', NULL, 65434, 'Robotic St', 'Jacksonville', 'FL', 348348342, 4564325456, 'deposit@gmail.com', NULL);
INSERT INTO `gsg17`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 4, 393842309, 'Hades', 'Gosh', '1990-01-04', '2011-01-01', NULL, 23454, 'Irony Ln', 'Fort Lauderdalle', 'FL', 949484823, 2384398498, 'contingency@gmail.com', NULL);
INSERT INTO `gsg17`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 5, 231934932, 'Neptune', 'Planet', '1990-01-05', '2012-01-01', NULL, 54321, 'Cyborg Rd', 'Tallahassee', 'FL', 129821843, 2387383113, 'test@gmail.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `gsg17`.`emp_hist`
-- -----------------------------------------------------
START TRANSACTION;
USE `gsg17`;
INSERT INTO `gsg17`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 1, '2000-05-01', 'i', 1, 52147, 'yes', 'moved', NULL);
INSERT INTO `gsg17`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 2, '2000-05-02', 'u', 2, 43743, 'yes', 'moved', NULL);
INSERT INTO `gsg17`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 3, '2000-05-03', 'd', 3, 39839, 'no', 'moved', NULL);
INSERT INTO `gsg17`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 4, '2000-05-04', 'i', 4, 39299, 'no', 'death', NULL);
INSERT INTO `gsg17`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_usr_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 5, '2000-05-05', 'u', 5, 84842, 'yes', 'moved', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `gsg17`.`dependent`
-- -----------------------------------------------------
START TRANSACTION;
USE `gsg17`;
INSERT INTO `gsg17`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 2, '2000-01-02', 123456789, 'John', 'Oliver', '1995-01-01', DEFAULT, '111 Olivion St', 'Tallahasee', 'FL', 987654321, 1234567890, 'o@gmail.com', NULL);
INSERT INTO `gsg17`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 3, '2000-01-03', 123345678, 'Orion', 'Belt', '1995-01-02', DEFAULT, '121 Belt Rd', 'Orlando', 'FL', 654356434, 6754438348, 'Belt@gmail.com', NULL);
INSERT INTO `gsg17`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 4, '2000-01-04', 143256534, 'Igna', 'Bravehart', '1995-01-03', DEFAULT, '543 Hart Rd', 'Miami', 'FL', 765432345, 7438734298, 'Hart@gmail.com', NULL);
INSERT INTO `gsg17`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 5, '2000-01-05', 234565432, 'Davius', 'Johns', '1995-01-04', DEFAULT, '289 Jons Rd', 'Jacksonville', 'FL', 123456432, 2838389930, 'Jonny@gmail.com', NULL);
INSERT INTO `gsg17`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 1, '2000-01-06', 345676544, 'Ignatius', 'Robers', '1995-01-05', DEFAULT, '919 Igni St', 'Atlanta', 'GA', 498249821, 4747301029, 'Ignit@gmail.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `gsg17`.`benefit`
-- -----------------------------------------------------
START TRANSACTION;
USE `gsg17`;
INSERT INTO `gsg17`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (1, 'dental', NULL);
INSERT INTO `gsg17`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (2, 'health', NULL);
INSERT INTO `gsg17`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (3, '401k', NULL);
INSERT INTO `gsg17`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (4, 'long term disability', NULL);
INSERT INTO `gsg17`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (5, 'dental', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `gsg17`.`plan`
-- -----------------------------------------------------
START TRANSACTION;
USE `gsg17`;
INSERT INTO `gsg17`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 1, 1, 'single', 23123, '2001-01-01', NULL);
INSERT INTO `gsg17`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 2, 2, 'spouse', 67413, '2001-02-02', NULL);
INSERT INTO `gsg17`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 3, 3, 'family', 76543, '2001-03-03', NULL);
INSERT INTO `gsg17`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 4, 4, 'single', 34562, '2001-04-04', NULL);
INSERT INTO `gsg17`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 5, 5, 'spouse', 12341, '2001-05-05', NULL);

COMMIT;

