> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Gabriel Gomez

### A1 # Requirements:

*Requirements:*

1. ERD
2. Data Dictionary
3. Screenshot of Code

#### README.md file should include the following items:

* Screenshot of Data Dictionary 
* Screenshot of ERD


#### Assignment Screenshots:

*Screenshot of ERD"*:

![ERD](img/ERD.png)

*Screenshot of Data Dictionary*:

![Data Dictionary Part A](img/DataDictionary1.png)

*Screenshot of Data Dictionary Cont*:

![Data Dictionary Part B](img/DataDictionary2.png)


