> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Gabriel Gomez

### A3 # Requirements:

*Requirements:*

1. Code
2. Screenshots of tables

#### README.md file should include the following items:

* Screenshot of populated tables
* Screenshot of Code



> This is a blockquote.
> 
#### Assignment Screenshots:

*Screenshot of Populated tables*:

![PopulatedTables](img/Code.png)

*Screenshot of First Block of Code*:

![First Block of Code](img/Block1.png)

*Screenshot of Second Block of Code*:

![Second Block of Code](img/Block2.png)

*Screenshot of Third block of Code*:

![Third Block of Code](img/Block3.png)


